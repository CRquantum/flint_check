module MyDiffEq

    use FLINT
    use, intrinsic :: IEEE_ARITHMETIC
    implicit none 
    
    ! user diff eq system
    type, extends(DiffEqSys) :: PM_ODE_Sys
        !Prim
        real(wp) :: Ka
        real(wp) :: Vmax0
        real(wp) :: Km
        real(wp) :: Vc0
        real(wp) :: FA1
        real(wp) :: KCP
        real(wp) :: KPC
        !Cov
        real(wp) :: wt
        real(wp) :: age
        !Sec
        real(wp) :: VM  
        real(wp) :: V 
    contains
        procedure :: F => YP
    end type PM_ODE_Sys    
contains      
    function YP(me, X, Y, Params)
    implicit none
    intrinsic :: size  
    class(PM_ODE_Sys), intent(inout) :: me !< Differential Equation object
    real(WP), intent(in) :: X
    real(WP), intent(in), dimension(me%n) :: Y 
    real(WP), intent(in), dimension(:), optional :: Params
    real(WP), dimension(size(Y)) :: YP
    !Diffeqs
    !write (6,*) 'ODEs called'
    YP(1) = -me%Ka*Y(1)
    YP(2) = me%Ka*Y(1) - me%VM/(me%Km*me%V+Y(2))*Y(2) - me%KCP*Y(2) + me%KPC*Y(3)
    YP(3) = me%KCP*Y(2) - me%KPC*Y(3)    
    !write (6,*) 'ODEs = ', YP
    return
    end function YP
          
end module MyDiffEq

module model
    use FLINT
    use MyDiffEq   
    implicit none
    ! Turn on the events
    logical, private, parameter :: EventsEnabled = .FALSE.
    logical, private, dimension(3), parameter :: EvMask = [.FALSE.,.FALSE.,.FALSE.] 
    ! Event step size
    real(WP), private, parameter :: evstepsz = 0.00001_WP
    ! event tolerance
    real(WP), private, dimension(3) :: evtol = [0.001_WP,1.0e-9_WP,1.0e-9_WP]
    ! scalar tolerance
    real(kind=wp), private, parameter :: rtol   = 1.0e-4_WP  ! originally it is 10^-11.
    real(kind=wp), private, parameter :: atol   = rtol*1.0e-3_WP    
    ! max no. of steps
    integer, private, parameter :: MAX_STEPS = 10**4 ! decreae this can increase speed.
    logical, private, parameter :: CONST_STEPSZ = .FALSE. 

    contains
    
    subroutine my_ode_init(me)
    class(PM_ODE_Sys) :: me
    
    me%n = 3 ! # of ODEs
    me%m = 0 ! # of events, 0, so turn it off.
    
    me%Ka = 6.51750402383093
    me%Vmax0 = 23.1441646658992
    me%Km = -4.83193907550415
    me%Vc0 = 3.48392293236760
    me%FA1 = 0.543133339226148
    me%KCP = 5.80211181194353 
    me%KPC = 8.18019402422159
    
    me%wt = 59.05249
    me%age = 17.57132
    
    me%VM = me%Vmax0 * me%wt**0.75
    me%V = me%Vc0 * me%wt   
    return
    end subroutine my_ode_init
    
    subroutine test()
    real(kind=wp) :: x0_test, xf_test
    real(kind=wp), dimension(3) :: y0_test, yf_test ! here neq=3    
    real(kind=wp) :: stepsz0, stepsz
    integer :: stifftestval, stiffstatus
    real(kind=wp), dimension(:), allocatable, target :: Xint, Xarr
    real(kind=wp), dimension(:,:), allocatable, target :: Yint, Yarr
    real(kind=wp), allocatable, dimension(:,:) :: EventStates
    character(len=:), allocatable :: errstr
    integer :: fcalls, naccpt, nrejct
    type(ERK_class) erkvar
    type(PM_ODE_Sys) :: My_obj    
    
    stepsz0 = 0.0E-3_wp    ! let FLINT compute the initial step-size    
    stifftestval = 1  ! check for stiffness and stop integration if stiff 
    
    stiffstatus = stifftestval
    stepsz = stepsz0   
    
    x0_test = 2.0_wp
    xf_test = 24.0_wp
    y0_test = [0.0_wp, 155.921971503913_wp, 101.637238186467_wp]
         
    write(6,*) 'integral range [x0, xf] =', x0_test, xf_test
    write(6,*) 'inital condition y0 =', y0_test
    
    call my_ode_init(My_obj) ! initalize the ODE system My_obj
    
    write(6,*) My_obj%n, My_obj%m, My_obj%Ka, My_obj%Vmax0, My_obj%Km, My_obj%Vc0, My_obj%FA1, My_obj%KCP, My_obj%KPC &
        , My_obj%wt, My_obj%age, My_obj%VM, My_obj%V 
    write(6,*) My_obj%F(0.0_WP,y0_test)
    
    call erkvar%Init(My_obj, MAX_STEPS, Method=ERK_DOP853, ATol=[atol], RTol=[rtol],&
        InterpOn=.false., EventsOn=.FALSE. ) 
    
    if (erkvar%status == FLINT_SUCCESS) then  
        write(6,*) 'FLINT Init success'      
        call erkvar%Integrate(x0_test, y0_test, xf_test, yf_test, StepSz=stepsz, UseConstStepSz=CONST_STEPSZ, &
            IntStepsOn=.false.,Xint = Xint, Yint = Yint, &
            EventStates=EventStates, EventMask = EvMask,StiffTest=stiffstatus)   
        if (erkvar%status == FLINT_SUCCESS) then   
            write(6,*) 'FLINT success'           
            call erkvar%Info(stiffstatus, errstr, nAccept=naccpt, nReject=nrejct, nFCalls=fcalls)        
            print *, fcalls, naccpt, nrejct        
        else
            write(6,*) 'FLINT fail with status: ', erkvar%status
            write(6,*) 'integral range [x0, xf] =', x0_test, xf_test
            write(6,*) 'y0 =', y0_test
            write(6,*) 'yf =', yf_test
            error stop
        endif         
          
    else
        write(6,*) 'init fail'
    endif    

    return
    end subroutine test

end module model
    

    program TestFLINT_CR
    use model   
    implicit none  
    call test()
    stop ('Program ends normally!')
    end program TestFLINT_CR


